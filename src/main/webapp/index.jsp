<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="root" value="<%=request.getContextPath()%>"></c:set>
<html>
    <head>
    	<title>wpfc</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <script type="text/javascript" src="${root }/static/js/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="${root }/static/js/jquery.cookie.js"></script>
        <script type="text/javascript" src="${root }/static/js/layer/layer.js"></script>
        <script type="text/javascript" src="${root }/static/js/date.js"></script>
        <link rel="stylesheet" href="${root }/static/js/layer/theme/default/layer.css" />
        <link rel="stylesheet" href="${root }/static/css/index.css?t=1.1.2" />
    </head>
    <body>
        <div class="header">
        	<h2>wpfc的页面学习第一步</h2>
        </div>
        <div class="body">
        	<div class="userlist">
       			<div class="userlist-header">用户列表<span class="userNum">0</span></div>
	        	<ul>
	        	</ul>
	        </div>
        	<div class="container">
        		<div class="content-header">
        			<div class="userlistImage"></div>
					<span class="msg_windows_title">消息窗口</span>
	        	</div>
	        	<div id="main" class="content-body">
	        		<!-- message -->
	        	</div>
	        	<div class="content-footer">
	        		<div class="send" style="display:none;">
			        	<button id="disconnect" class="btn btn-danger">断开</button>
			        	<button id="send" class="btn btn-primary">发送</button>
			        	<input type="text" name="content" class="content"/>
	        		</div>
	        		<div class="connect">
			        	<button id="connect" class="btn btn-success">连接</button>
			        	<input type="text" name="url" class="url" value="" placeholder="测试你自己的服务器"/>
	        		</div>
		        </div>
        	</div>
        </div>
        <div class="footer">
        wpfc
        	<!-- <span class="input-group-addon" style="color:orange;">昵称</span>
            <input id="inp_nickname" type="text" maxlength="10" class="form-control">
            <span id="btn_getnick" class="input-group-btn">
            	<button class="btn btn-default" type="button"><span style="color:orange;" class="glyphicon glyphicon-refresh"></span></button>
           	</span>
            <span id="btn_inroom" class="input-group-btn">
            	<button class="btn btn-default" type="button"><span style="color:green;" class="glyphicon glyphicon-home"></span></button>
            </span> -->
        </div>
    </body>
    <script>
		function getContentSize() {
            var wh = document.documentElement.clientHeight; 
            var eh = 250;
            ch = (wh - eh) + "px";
            var userlistHeight = (wh - eh + 74) + "px";
            document.getElementById( "main" ).style.height = ch;
            $(".userlist ul").css("height", userlistHeight);
        }
        window.onload = getContentSize;
        window.onresize = getContentSize;
	</script>
    <script type="text/javascript" src="${root }/static/js/index.js?t=1.1.4"></script>
</html>
