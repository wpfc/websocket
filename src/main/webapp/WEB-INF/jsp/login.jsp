<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<title>欢迎来到登录页面！</title>
</head>
<body>
	<h1>欢迎来到登录页面！</h1>
	<div id="qrcode" class="qecode-img" style="border:none;"></div>
	<div id="message"></div>
</body>
<script type="text/javascript" src="http://cdn.static.runoob.com/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="http://static.runoob.com/assets/qrcode/qrcode.min.js" ></script>
<script type="text/javascript">
	var uuid = "${uuid}";
	var qrcode = new QRCode(document.getElementById("qrcode"), {
		width : 168,
		height : 168
	});
	qrcode.makeCode("http://192.168.0.107:8080/springmvc-demo/login/" + uuid);
	
	var websocket = null;
	
	initWebscoket();
	function initWebscoket(){
        websocket = new WebSocket("ws://localhost:8080/springmvc-demo/"+uuid); 
        websocket.onopen = function(evt) { 
        }; 
        websocket.onclose = function(evt) { 
        }; 
        websocket.onmessage = function(evt) { 
            var data = JSON.parse(evt.data);
            if(1 == data.status){
            	$("#message").append("<p>已扫码</p>");
            }else if(2 == data.status){
            	$("#message").append("<p>已登录</p>");
            }else if(3 == data.status){
            	$("#message").append("<p>已拒绝</p>");
            }
        }; 
        websocket.onerror = function(evt) { 
        }; 
	}
</script>
</html>