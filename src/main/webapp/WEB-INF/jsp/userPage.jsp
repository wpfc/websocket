<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="root" value="<%= request.getContextPath() %>"></c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>欢迎来到用户页面！</title>
</head>
<body>
	<h1>欢迎来到用户页面！${msg }</h1>
	<img alt="" src="<c:url value='/static/img/404.jpg'/>">
</body>
<script type="text/javascript">
	var root = '${root}';
	console.info(root);
</script>
</html>