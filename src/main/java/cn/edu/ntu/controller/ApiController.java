package cn.edu.ntu.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.edu.ntu.entity.HttpResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("api")
public class ApiController {

	/**
	 * @ApiOperation 的httpMethod 的优先级大于@RequestMapping method
	 * 同时设置时以前者为准
	 * 参数上加上@ApiParam()之后无法获取到传递的参数 ???
	 */
	@ApiOperation(value = "测试", httpMethod = "GET", notes = "测试")
	@RequestMapping(value="/test", method=RequestMethod.GET)
	public HttpResult test(@ApiParam(required = true, name = "token", value = "秘钥") String token){
		return HttpResult.ok().data(token).build();
	}
	
}
