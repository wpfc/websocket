package cn.edu.ntu.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.edu.ntu.common.Aspect;
import cn.edu.ntu.entity.User;
import cn.edu.ntu.mapper.UserMapper;

@Aspect
@Controller
public class UserController {

	@Autowired
	private UserMapper userMapper;
	
	@RequestMapping("toUserPage")
	public ModelAndView toUserPage(HttpServletRequest request){
		ServletContext context = request.getServletContext();
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("userPage");
		mv.addObject("msg", "helloworld");
		return mv;
	}
	
	@RequestMapping("/getUserInfo")
	@ResponseBody
	public User getUserInfo(){
		User user = userMapper.selectByPrimaryKey(1L);
		return user;
	}
	
}
