package cn.edu.ntu.mapper;

import java.util.List;

import cn.edu.ntu.entity.Area;

public interface AreaMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Area record);

    int insertSelective(Area record);

    Area selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Area record);

    int updateByPrimaryKey(Area record);
    
    List<Area> queryCity();
    
    List<Area> queryAllCity();
}