package cn.edu.ntu.websocket;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/{uuid}")
public class WebSocketServer {

	public static Map<String, Session> sessionMap = new ConcurrentHashMap<String, Session>(); 
	
	@OnOpen
	public void onOpen(Session session, @PathParam(value = "uuid") String uuid) throws IOException {
		//存储用户的信息
		sessionMap.put(uuid, session);
	}
	 
	@OnMessage
	public String onMessage(String message) {
		//以下代码省略...
		return message;
	}
	 
	@OnClose
	public void onClose(Session session, CloseReason reason) {
		//以下代码省略...
	} 
	
}
