package cn.edu.ntu.utils;

public final class Constant {

	public interface MESSAGE_TYPE{
		/**
		 * 重复登录处理
		 */
		static final String REPEAT_LOGIN = "-1";
		/**
		 * 公共空间消息获取
		 */
		static final String LOGIN_RETURN = "0";
		/**
		 * 公共空间消息发送
		 */
		static final String NORMAL = "1";
		/**
		 * 进入聊天
		 */
		static final String LOGIN = "2";
		/**
		 * 退出聊天
		 */
		static final String LOGIN_OUT = "3";
		/**
		 * 一对一的聊天记录
		 */
		static final String ONE_RECORDS = "4";
		/**
		 * 一对一的消息
		 */
		static final String ONE = "5";
		
		
	}
	

}
