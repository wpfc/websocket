package cn.edu.ntu.constant;

public enum TypeEnum {

	/**
	 * 登录连接
	 */
	CONNECT("connect", 1),
	/**
	 * 心跳连接
	 */
	HEART("heart", 2),/**
	 * 群发消息
	 */
	ALL("all", 3),
	/**
	 * 一对一消息
	 */
	ONE("one", 4),
	/**
	 * 获取一对一聊天记录
	 */
	ONE_RECORDS("one_records", 5);
	
	private String name;
	
	private int code;
	
	private TypeEnum(String name, int code){
		this.name = name;
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
}

