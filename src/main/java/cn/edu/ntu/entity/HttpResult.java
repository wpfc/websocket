package cn.edu.ntu.entity;

public class HttpResult {

	private static String SUCCESS = "200";
	private static String FAILURE = "999";
	
	/**
	 * 状态   200：成功    999：失败
	 */
	private String status;
	/**
	 * 消息
	 */
	private String msg;
	/**
	 * 返回的数据
	 */
	private Object data;
	
	public HttpResult(String status, String msg, Object data) {
		this.status = status;
		this.msg = msg;
		this.data = data;
	}

    public static Builder ok(){
    	return new Builder().status(SUCCESS);
    }
    
    public static Builder error(){
    	return new Builder().status(FAILURE);
    }

	public static class Builder{
		private String status;
		private String msg;
		private Object data;
		
		public Builder status(String status) {
			this.status = status;
			return this;
		}

		public Builder msg(String msg) {
			this.msg = msg;
			return this;
		}

		public Builder data(Object data) {
			this.data = data;
			return this;
		}

		public HttpResult build(){
			return new HttpResult(status, msg, data);
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
	
}
