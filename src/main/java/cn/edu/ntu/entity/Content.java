package cn.edu.ntu.entity;

import java.util.Date;

public class Content {

	/**
	 * 消息类型
	 */
	private String type;
	/**
	 * 消息发送者
	 */
	private String id;
	/**
	 * 消息发送者昵称
	 */
	private String name;
	/**
	 * 消息接收者
	 */
	private String toId;
	/**
	 * 消息接收者昵称
	 */
	private String toName;
	/**
	 * 消息
	 */
	private String content;
	/**
	 * 创建时间
	 */
	private Date createTime;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getToId() {
		return toId;
	}
	public void setToId(String toId) {
		this.toId = toId;
	}
	public String getToName() {
		return toName;
	}
	public void setToName(String toName) {
		this.toName = toName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	
}
