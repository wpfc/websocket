package cn.edu.ntu.entity;

import java.util.Date;
import java.util.List;

public class WSResult {

	/**
	 * 消息类型
	 */
	private String type;
	/**
	 * 消息发送自xx
	 */
	private String from;
	/**
	 * 消息发送至xx
	 */
	private String to;
	/**
	 * 消息内容
	 */
	private String content;
	/**
	 * 时间
	 */
	private Date sendTime;
	/**
	 * 用户列表
	 */
	private List<WSUser> userList;
	
	private List<Content> contentList;
	/**
	 * 放间名称
	 */
	private String roomName;
	/**
	 * 当前用户id
	 */
	private String id;
	/**
	 * 当前用户昵称
	 */
	private String name;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public List<WSUser> getUserList() {
		return userList;
	}

	public void setUserList(List<WSUser> userList) {
		this.userList = userList;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Content> getContentList() {
		return contentList;
	}

	public void setContentList(List<Content> contentList) {
		this.contentList = contentList;
	}
	
}
