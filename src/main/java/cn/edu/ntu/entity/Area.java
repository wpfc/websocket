package cn.edu.ntu.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author wpfc
 * @JsonIgnoreProperties(value = { "handler" })用于处理json解析报错
 * 
 * com.fasterxml.jackson.databind.exc.InvalidDefinitionException: 
 * No serializer found for class org.apache.ibatis.executor.loader.javassist.JavassistProxyFactory$EnhancedResultObjectProxyImpl 
 * and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS) 
 * (through reference chain: java.util.ArrayList[0]->cn.edu.ntu.entity.Area_$$_jvst1fb_0["subAreas"]->java.util.ArrayList[0]->
 * cn.edu.ntu.entity.Area_$$_jvst1fb_0["subAreas"]->java.util.ArrayList[0]->cn.edu.ntu.entity.Area_$$_jvst1fb_0["handler"])
 *
 */
@JsonIgnoreProperties(value = { "handler" })
public class Area implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	private Integer id;
	/**
	 * 地区名称
	 */
    private String name;
    /**
     * 地区编码
     */
    private Integer areaId;
    /**
     * 上一级地区编码
     */
    private Integer parentId;
    /**
    * 下一级地区列表
    */
    private List<Area> subAreas;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

	public List<Area> getSubAreas() {
		return subAreas;
	}

	public void setSubAreas(List<Area> subAreas) {
		this.subAreas = subAreas;
	}
    
}