/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50528
Source Host           : localhost:3306
Source Database       : demo

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2018-02-23 11:26:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `area_id` int(20) DEFAULT NULL,
  `parent_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of area
-- ----------------------------
INSERT INTO `area` VALUES ('1', '杭州', '1', null);
INSERT INTO `area` VALUES ('2', '南京', '2', null);
INSERT INTO `area` VALUES ('3', '上海', '3', null);
INSERT INTO `area` VALUES ('4', '苏州', '4', null);
INSERT INTO `area` VALUES ('5', '上城区', '10001', '1');
INSERT INTO `area` VALUES ('6', '下城区', '10002', '1');
INSERT INTO `area` VALUES ('7', '江干区', '10003', '1');
INSERT INTO `area` VALUES ('8', '拱墅区', '10004', '1');
INSERT INTO `area` VALUES ('9', '西湖区', '10005', '1');
INSERT INTO `area` VALUES ('10', '滨江区', '10006', '1');
INSERT INTO `area` VALUES ('11', '萧山区', '10007', '1');
INSERT INTO `area` VALUES ('12', '余杭区', '10008', '1');
INSERT INTO `area` VALUES ('13', '四季青', '10101', '10001');
INSERT INTO `area` VALUES ('14', '复兴', '10102', '10001');
INSERT INTO `area` VALUES ('15', '南星桥', '10103', '10001');
INSERT INTO `area` VALUES ('16', '近江', '10104', '10001');
INSERT INTO `area` VALUES ('17', '望江门', '10105', '10001');
INSERT INTO `area` VALUES ('18', '清泰(城战)', '10106', '10001');
INSERT INTO `area` VALUES ('19', '吴山', '10107', '10001');
INSERT INTO `area` VALUES ('20', '菜市桥', '10108', '10001');
INSERT INTO `area` VALUES ('21', '解百', '10109', '10001');
INSERT INTO `area` VALUES ('22', '马市街', '10110', '10001');
INSERT INTO `area` VALUES ('23', '河坊街', '10111', '10001');
INSERT INTO `area` VALUES ('24', '武林', '10201', '10002');
INSERT INTO `area` VALUES ('25', '潮鸣', '10202', '10002');
INSERT INTO `area` VALUES ('26', '庆春', '10203', '10002');
INSERT INTO `area` VALUES ('27', '宝善', '10204', '10002');
INSERT INTO `area` VALUES ('28', '三塘', '10205', '10002');
INSERT INTO `area` VALUES ('29', '石桥', '10206', '10002');
INSERT INTO `area` VALUES ('30', '东新', '10207', '10002');
INSERT INTO `area` VALUES ('31', '和平', '10208', '10002');
INSERT INTO `area` VALUES ('32', '西湖文化广场', '10209', '10002');
INSERT INTO `area` VALUES ('33', '朝晖', '10210', '10002');
INSERT INTO `area` VALUES ('34', '孩儿巷', '10211', '10002');
INSERT INTO `area` VALUES ('35', '湖滨', '10212', '10002');
INSERT INTO `area` VALUES ('36', '流水苑', '10213', '10002');
INSERT INTO `area` VALUES ('37', '莫衙营', '10214', '10002');
INSERT INTO `area` VALUES ('38', '天水', '10215', '10002');
INSERT INTO `area` VALUES ('39', '长庆', '10216', '10002');
INSERT INTO `area` VALUES ('40', '下沙物美', '10301', '10003');
INSERT INTO `area` VALUES ('41', '下沙金沙湖', '10302', '10003');
INSERT INTO `area` VALUES ('42', '钱江新城', '10303', '10003');
INSERT INTO `area` VALUES ('43', '四季青', '10304', '10003');
INSERT INTO `area` VALUES ('44', '采荷', '10305', '10003');
INSERT INTO `area` VALUES ('45', '南肖埠', '10306', '10003');
INSERT INTO `area` VALUES ('46', '九堡', '10307', '10003');
INSERT INTO `area` VALUES ('47', '闸弄口', '10308', '10003');
INSERT INTO `area` VALUES ('48', '三里亭', '10309', '10003');
INSERT INTO `area` VALUES ('49', '火车东站', '10310', '10003');
INSERT INTO `area` VALUES ('50', '景芳', '10311', '10003');
INSERT INTO `area` VALUES ('51', '下沙沿江', '10312', '10003');
INSERT INTO `area` VALUES ('52', '彭埠', '10313', '10003');
INSERT INTO `area` VALUES ('53', '下沙江滨', '10314', '10003');
INSERT INTO `area` VALUES ('54', '拱宸桥', '10401', '10004');
INSERT INTO `area` VALUES ('55', '大关', '10402', '10004');
INSERT INTO `area` VALUES ('56', '桥西', '10403', '10004');
INSERT INTO `area` VALUES ('57', '卖鱼桥', '10404', '10004');
INSERT INTO `area` VALUES ('58', '瓜山', '10405', '10004');
INSERT INTO `area` VALUES ('59', '祥符', '10406', '10004');
INSERT INTO `area` VALUES ('60', '米市巷', '10407', '10004');
INSERT INTO `area` VALUES ('61', '万达', '10408', '10004');
INSERT INTO `area` VALUES ('62', '长乐', '10409', '10004');
INSERT INTO `area` VALUES ('63', '文新', '10501', '10005');
INSERT INTO `area` VALUES ('64', '益乐', '10502', '10005');
INSERT INTO `area` VALUES ('65', '翠苑', '10503', '10005');
INSERT INTO `area` VALUES ('66', '申花', '10504', '10005');
INSERT INTO `area` VALUES ('67', '九莲', '10505', '10005');
INSERT INTO `area` VALUES ('68', '文教', '10506', '10005');
INSERT INTO `area` VALUES ('69', '学军', '10507', '10005');
INSERT INTO `area` VALUES ('70', '古荡', '10508', '10005');
INSERT INTO `area` VALUES ('71', '黄龙', '10509', '10005');
INSERT INTO `area` VALUES ('72', '三敦', '10510', '10005');
INSERT INTO `area` VALUES ('73', '转塘', '10511', '10005');
INSERT INTO `area` VALUES ('74', '西溪', '10512', '10005');
INSERT INTO `area` VALUES ('75', '保俶路', '10513', '10005');
INSERT INTO `area` VALUES ('76', '文苑', '10514', '10005');
INSERT INTO `area` VALUES ('77', '骆家庄', '10515', '10005');
INSERT INTO `area` VALUES ('78', '西兴', '10601', '10006');
INSERT INTO `area` VALUES ('79', '浦沿', '10601', '10006');
INSERT INTO `area` VALUES ('80', '一桥南', '10602', '10006');
INSERT INTO `area` VALUES ('81', '滨盛', '10603', '10006');
INSERT INTO `area` VALUES ('82', '四桥南', '10604', '10006');
INSERT INTO `area` VALUES ('83', '区政府', '10605', '10006');
INSERT INTO `area` VALUES ('84', '奥体', '10606', '10006');
INSERT INTO `area` VALUES ('85', '长河', '10607', '10006');
INSERT INTO `area` VALUES ('86', '开发区', '10701', '10007');
INSERT INTO `area` VALUES ('87', '北干', '10702', '10007');
INSERT INTO `area` VALUES ('88', '城厢', '10703', '10007');
INSERT INTO `area` VALUES ('89', '宁围', '10704', '10007');
INSERT INTO `area` VALUES ('90', '蜀山', '10705', '10007');
INSERT INTO `area` VALUES ('91', '临平', '10801', '10008');
INSERT INTO `area` VALUES ('92', '老余杭', '10802', '10008');
INSERT INTO `area` VALUES ('93', '良渚', '10803', '10008');
INSERT INTO `area` VALUES ('94', '闲林', '10804', '10008');
INSERT INTO `area` VALUES ('95', '未来科技城', '10805', '10008');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `age` int(5) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'wpfc', '123456', '26', '');
