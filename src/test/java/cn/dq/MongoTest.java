package cn.dq;

import java.io.IOException;
import java.util.Properties;

import org.bson.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * 需要引入  spring-test.jar
 * @author weipeng
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)   
@ContextConfiguration(locations = {"classpath:spring/applicationContext.xml"})  
public class MongoTest {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Test
	public void test(){
		MongoClient client = new MongoClient("localhost", 27017);
		MongoDatabase database = client.getDatabase("dq");
		
		MongoCollection<Document> documents = database.getCollection("form");
		System.out.println("documents count : " + documents.count());
	}
	
	@Test
	public void testByApplicaiotn(){
		ApplicationContext application = new ClassPathXmlApplicationContext("classpath*:spring/applicationContext.xml");
		
		Resource resource = application.getResource("classpath:db.properties");
		try {
			Properties props = new Properties();
			props.load(resource.getInputStream());
			System.out.println("username : " + props.getProperty("db.username"));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ConfigurableApplicationContext context = (ConfigurableApplicationContext) application;
		DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) context.getBeanFactory();
		
		MongoTemplate mongoTemplate = application.getBean(MongoTemplate.class);
		System.out.println(mongoTemplate);
		long count = mongoTemplate.count(new Query(Criteria.where("zan").is(0)), "form");
		System.out.println("documents count : " + count);
	}
	
	@Test
	public void testMongoTemplate(){
		System.out.println(mongoTemplate);
		long count = mongoTemplate.count(new Query(Criteria.where("zan").is(0)), "form");
		System.out.println("documents count : " + count);
	}
	
}
