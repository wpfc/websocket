package cn.dq;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.edu.ntu.entity.Area;
import cn.edu.ntu.entity.User;
import cn.edu.ntu.mapper.AreaMapper;
import cn.edu.ntu.mapper.UserMapper;
import cn.edu.ntu.utils.JsonUtils;

public class MybatisTest {

	@Test
	public void test(){
		ApplicationContext application = new ClassPathXmlApplicationContext("classpath*:spring/applicationContext.xml");
		UserMapper userMapper = application.getBean(UserMapper.class);
		User user = userMapper.selectByPrimaryKey(1L);
		System.out.println("username : " + user.getUserName());
		
		AreaMapper areaMapper = application.getBean(AreaMapper.class);
		List<Area> areaList = areaMapper.queryCity();
		System.out.println(areaList.size());
		System.out.println(areaList.get(0).getSubAreas().size());
		System.out.println(JsonUtils.objectToJson(areaList));
		
//		MongoTemplate mongoTemplate = application.getBean(MongoTemplate.class);
//				System.out.println(mongoTemplate);
//		long count = mongoTemplate.count(new Query(Criteria.where("zan").is(0)), "form");
//		System.out.println("documents count : " + count);
		
	}
	
}
