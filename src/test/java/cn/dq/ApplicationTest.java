package cn.dq;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

public class ApplicationTest {
	
	@Test
	public void test(){
		try {
			HttpClient client = new DefaultHttpClient();                //构建一个Client
			HttpPost post = new HttpPost("http://shop.luruiqi.cc:8080/login");    //构建一个POST请求
			//构建表单参数
			List<NameValuePair> formParams = new ArrayList<NameValuePair>();
			formParams.add(new BasicNameValuePair("phone", "17723507130"));
			formParams.add(new BasicNameValuePair("password", "e10adc3949ba59abbe56e057f20f883e"));
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, "UTF-8");//将表单参数转化为“实体”
			post.setEntity(entity);        //将“实体“设置到POST请求里
			            
			HttpResponse response = client.execute(post);//提交POST请求
			HttpEntity result = response.getEntity();//拿到返回的HttpResponse的"实体"
			String content = EntityUtils.toString(result);;            //用httpcore.jar提供的工具类将"实体"转化为字符串打印到控制台
			System.out.println(content);
			if(content.contains("登陆成功")){
				System.out.println("登陆成功！！！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static String cookieVal="";
			  
    public static String Get(String url_get,String str_param_url,String charset,String cookie) throws IOException  {  
    	
    	String cookieVal = null;
    	
        // 拼凑get请求的URL字串，使用URLEncoder.encode对特殊和不可见字符进行编码  
        //    String getURL = GET_URL + "?username="  + URLEncoder.encode("fat man", "utf-8");  
        String getURL = url_get + "?" + str_param_url;  
        URL getUrl = new URL(getURL);  
        // 根据拼凑的URL，打开连接，URL.openConnection函数会根据URL的类型，  
        // 返回不同的URLConnection子类的对象，这里URL是一个http，因此实际返回的是HttpURLConnection  
        HttpURLConnection connection = (HttpURLConnection) getUrl  
                .openConnection();  
  
        if (cookie != null) {  
            //发送cookie信息上去，以表明自己的身份，否则会被认为没有权限  
            println("set cookieVal = [" + cookie + "]");  
            connection.setRequestProperty("Cookie", cookie);  
        }  
  
        // 进行连接，但是实际上get request要在下一句的connection.getInputStream()函数中才会真正发到  
        // 服务器  
        connection.connect();  
        // 取得输入流，并使用Reader读取  
        BufferedReader reader = new BufferedReader(new InputStreamReader(  
                connection.getInputStream(),charset));  
        
        System.out.println("Contents of get request:");  
        String lines;  
        while ((lines = reader.readLine()) != null)  {  
             System.out.println(lines);  
        }  
        println(" ");  
        reader.close();  
        // 断开连接  
        connection.disconnect();  
        
        return cookieVal;
    }  
  
    public static String Post(String url_post,String str_param_body,String charset,boolean b_flag,String cookies) throws IOException  {  
        // Post请求的url，与get不同的是不需要带参数  
        URL postUrl = new URL(url_post);  
        // 打开连接  
        HttpURLConnection connection = (HttpURLConnection) postUrl  
                .openConnection();  
        // Output to the connection. Default is  
        // false, set to true because post  
        // method must write something to the  
        // connection  
        // 设置是否向connection输出，因为这个是post请求，参数要放在  
        // http正文内，因此需要设为true  
        if("" != cookies){  
            connection.setRequestProperty("Cookie", cookies);  
        }  
  
        connection.setDoOutput(true);  
        // Read from the connection. Default is true.  
        connection.setDoInput(true);  
        // Set the post method. Default is GET  
        connection.setRequestMethod("POST");  
        // Post cannot use caches  
        // Post 请求不能使用缓存  
        connection.setUseCaches(false);  
        // This method takes effects to  
        // every instances of this class.  
        // URLConnection.setFollowRedirects是static函数，作用于所有的URLConnection对象。  
        // connection.setFollowRedirects(true);  
  
        // This methods only  
        // takes effacts to this  
        // instance.  
        // URLConnection.setInstanceFollowRedirects是成员函数，仅作用于当前函数  
        connection.setInstanceFollowRedirects(b_flag);  
        // Set the content type to urlencoded,  
        // because we will write  
        // some URL-encoded content to the  
        // connection. Settings above must be set before connect!  
        // 配置本次连接的Content-type，配置为application/x-www-form-urlencoded的  
        // 意思是正文是urlencoded编码过的form参数，下面我们可以看到我们对正文内容使用URLEncoder.encode  
        // 进行编码  
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
//        connection.setRequestProperty("Content-Type", "text/plain;charset=UTF-8"); 
        
        // 连接，从postUrl.openConnection()至此的配置必须要在connect之前完成，  
        // 要注意的是connection.getOutputStream会隐含的进行connect。  
        connection.connect();  
        DataOutputStream out = new DataOutputStream(connection  
                .getOutputStream());  
        // The URL-encoded contend  
        // 正文，正文内容其实跟get的URL中'?'后的参数字符串一致  
        //    String content = "userName=" + URLEncoder.encode("console", "utf-8");  
        //    content = content + "&password=" + URLEncoder.encode("12345678", "utf-8");  
  
        println("http param body = [" + str_param_body + "]");  
        // DataOutputStream.writeBytes将字符串中的16位的unicode字符以8位的字符形式写道流里面  
        out.writeBytes(str_param_body);  
  
        out.flush();  
  
        // 取得cookie，相当于记录了身份，供下次访问时使用  
        //    cookieVal = connection.getHeaderField("Set-Cookie").split(";")[0]  
        cookieVal = connection.getHeaderField("Set-Cookie");  
        println("get cookieVal = [" + cookieVal  + "]");  
  
        out.close(); // flush and close  
        BufferedReader reader = new BufferedReader(new InputStreamReader(  
                connection.getInputStream(),charset));  
        String line;  
        System.out.println("Contents of post request:");  
        while ((line = reader.readLine()) != null)  {  
            System.out.println(line);  
        }  
        println(" ");
  
        reader.close();  
        connection.disconnect();  
  
        return cookieVal; 
    }

	private static void println(String string) {
		System.out.println(string);
	}  

	public static void main(String[] args) throws Exception{
//		String param = "client_id=c3cef7c66a1843f8b3a9e6a1e3160e20&grant_type=password&timestamp=1516001495837&source=com.zhihu.web&signature=bb2ec3c51c4ad8e0e77fe5bb8ed24cbcb98d26bf&username=2570721221@qq.com&password=weipeng.123&captcha=&lang=en&ref_source=homepage&utm_source=";
//		String cookie_login  = Post("https://www.zhihu.com/api/v3/oauth/sign_in", param, "utf-8", false, cookieVal);
//		println(cookie_login);
//		Get("https://www.zhihu.com/people/wei-peng-59-55/activities","","utf-8",cookie_login);
		
		String cookieVal = Post("http://shop.luruiqi.cc:8080/login", "phone=17723507130&password=e10adc3949ba59abbe56e057f20f883e", "utf-8", false, null);
		println(cookieVal);
		Get("http://shop.luruiqi.cc:8080/user/userInfo", null, "utf-8", cookieVal);
	}
	
	
	
}
