package cn.dq;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class IpTets {

	public static void main(String[] args) throws Exception{
		
		String urlPath = "http://ip.taobao.com/service/getIpInfo.php?ip=120.77.60.6";
		
		URL url = new URL(urlPath);
		HttpURLConnection connect = (HttpURLConnection) url.openConnection();
		connect.setRequestMethod("GET");
		connect.setDoOutput(false);
		connect.setDoInput(true);  
		//设置连接超时时间和读取超时时间
		connect.setConnectTimeout(10000);
		connect.setReadTimeout(10000);
		connect.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        
		connect.connect();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream())) ;
		StringBuffer sb = new StringBuffer();
		String line;
		while((line = reader.readLine()) != null){
			sb.append(line);
		}
		
//		System.out.println(startGet(urlPath));
		System.out.println(sb.toString());
	}
	
	
	private static String startGet(String path){
        BufferedReader in = null;        
        StringBuilder result = new StringBuilder(); 
        try {
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            //Get请求不需要DoOutPut
            conn.setDoOutput(false);
            conn.setDoInput(true);
            //设置连接超时时间和读取超时时间
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //连接服务器  
            conn.connect();  
            // 取得输入流，并使用Reader读取  
            in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //关闭输入流
        finally{
            try{
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result.toString();
    }
}
